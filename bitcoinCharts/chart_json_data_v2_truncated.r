# https://projecteuler.net/problem=31

# install.packages("rjson")

library("rjson")


ptm <- proc.time()

jsonFiles <- list.files("../blockdata", pattern="*.json", full.names=TRUE)

aggregatedData <- list()

for (jsonFile in jsonFiles) {
    print(sprintf("Processing File: %s", jsonFile))
    jsonData <- fromJSON(file=jsonFile)

    for (blockData in jsonData) {
        medianTime <- blockData$mediantime
        date <- strftime(as.POSIXct(medianTime, origin="1970-01-01"), format="%d/%m/%Y")

        txs <- blockData$transactions
        totalFees <- 0
        for (tx in txs) {
            totalFees <- totalFees + tx$fees[1]
        }

        if (is.null(aggregatedData[[date]])) {
            aggregatedData[[date]] <- list(nTx=0, totalFees=0, blocks=0)
        } else {
        }

        aggregatedData[[date]][['nTx']] <- aggregatedData[[date]][['nTx']] + blockData$nTx
        aggregatedData[[date]][['totalFees']] <- aggregatedData[[date]][['totalFees']] + totalFees
        aggregatedData[[date]][['blocks']] <- aggregatedData[[date]][['blocks']] + 1
    }
    
    

}


# prepare data for charting
dates = c()
nTx = c()
totalFees = c()
blocks = c()

# remove first and last of aggregatedData as they will be incomplete data for those days
for (name in tail(head(names(aggregatedData), -1), -1)) {
    dates <- c(dates, name)
    nTx <- c(nTx, aggregatedData[[name]][['nTx']])
    totalFees <- c(totalFees, aggregatedData[[name]][['totalFees']])
    blocks <- c(blocks, aggregatedData[[name]][['blocks']])
}


nTxData <- data.frame(name=dates, value=nTx)
totalFeesData <- data.frame(name=dates, value=totalFees)
blocksData <- data.frame(name=dates, value=blocks)
meanBlocks <- sum(blocks)/length(blocks)

# charting
pdf("bitcoinCharts/bitcoinData_R_truncated.pdf")
par(mfrow=c(3,1))
barplot(height=nTxData$value, names=nTxData$name, main="No of Transactions per Day", ylab="No of Transactions", xlab="Date")
barplot(height=totalFeesData$value, names=totalFeesData$name, main="Total Fees per Day", ylab="Total Fees (Bitcoin)", xlab="Date")
barplot(height=blocksData$value, names=blocksData$name, main="Blocks per Day", ylab="Total Blocks", xlab="Date")
abline(h=meanBlocks,col="blue")
dev.off()

print(proc.time() - ptm)

#    user  system elapsed
#   27.00    1.76   29.06
