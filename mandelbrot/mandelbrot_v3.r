# https://www.r-bloggers.com/2016/10/creating-an-image-of-a-matrix-in-r-using-image/

ptm <- proc.time()

MAX_ITERATIONS = 100
REAL_START = -2.1
REAL_END = 0.6
IMAGINARY_START = -1.35
IMAGINARY_END = 1.35

HEIGHT = 4000
WIDTH = floor((HEIGHT * (REAL_END - REAL_START) / (IMAGINARY_END - IMAGINARY_START)))

cols=colorRampPalette(c("blue","yellow","red","black"))(MAX_ITERATIONS)

calcIterations <- function(c) {
    z = 0
    for (n in (0:MAX_ITERATIONS)) {
        if (abs(z) > 2) {
            return(n)
        }
        z = z**2 + c
    }
    return(MAX_ITERATIONS)
}

m = matrix(, ncol = length((0:HEIGHT)), nrow = length((0:WIDTH)))

for (x in (0:WIDTH)) {
    for (y in (0: HEIGHT)) {
        real = REAL_START + (x/WIDTH) * (REAL_END - REAL_START)
        imaginary = IMAGINARY_START + (y/HEIGHT) * (IMAGINARY_END - IMAGINARY_START)

        c = complex(real=real, imaginary=imaginary)
        iterations = calcIterations(c)
        m[x + 1,y + 1] = iterations
    }
}

pdf("mandelbrot/output/mandelbrot_v3_4K.pdf")
par(mar=c(0,0,0,0))
image(m, col=cols, useRaster=TRUE, axes=FALSE)
dev.off()

print(proc.time() - ptm)

#    Error: cannot allocate vector of size 488.3 Mb

# for height = 4000
#    user  system elapsed
#  337.31    0.43  339.17