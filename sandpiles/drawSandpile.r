cols=colorRampPalette(c("white","#73C2FB","#0080FF","#111E6C"))(4)

draw <- function(arr, filename) {
    pdf(paste("sandpiles/output/", filename, ".pdf", sep=""))
    par(mar=c(0,0,0,0))
    image(arr, col=cols, useRaster=TRUE, axes=FALSE, zlim=c(0,3))
    dev.off()
}
