source("sandpiles/abelianHelper.r")
source("sandpiles/drawSandpile.r")

ptm <- proc.time()

INIITIAL_SAND <- 50000
CHART_SIZE <- floor(0.9 * sqrt(INIITIAL_SAND))

m <- matrix(0, nrow=CHART_SIZE, ncol=CHART_SIZE)
m <- addSandToMiddle(m, INIITIAL_SAND)

stabm <- stabilize(m)

draw(stabm, paste("sandpile_", INIITIAL_SAND, sep=""))

print(proc.time() - ptm)

# 1000
#   user  system elapsed 
#    0.04    0.01    0.06 

# 50000
#    user  system elapsed
#   62.09    0.00   62.13

# 250000
#    user  system elapsed
# 1412.31    0.16 1412.78

# 500000
#    user  system elapsed
# 6091.86   17.97 6118.37