source("sandpiles/abelianHelper.r")
source("sandpiles/drawSandpile.r")

ptm <- proc.time()

n <- 400

cmax <- matrix(MAX_PILE, nrow=n, ncol=n)

stab2cmax <- stabilize(2 * cmax)
print("calculated stab2cmax")

identity <- stabilize((cmax - stab2cmax) + cmax)

draw(identity, paste("identity_", n, "x", n, sep=""))

print(proc.time() - ptm)

# 3x3
#    user  system elapsed
#    0.08    0.02    0.10

# 50x50
#    user  system elapsed
#    1.89    0.00    1.89

# 75x75
#    user  system elapsed
#    8.17    0.00    8.17

# 100x100
#    user  system elapsed
#   23.95    0.03   24.02

# 250x250
#    user  system elapsed
#  874.26    0.03  874.75

# 400
#    user  system elapsed
# 5923.18    7.22 5938.51