MAX_PILE = 3

stabilize <- function(arr) {
    shape <- dim(arr) # shape[1] = x_lim, shape[2] = y_lim
    x_lim <- shape[1]
    y_lim <- shape[2]

    while(any(arr > MAX_PILE, na.rm = TRUE)) {
        for (x in (1:x_lim)) {
            for (y in (1:y_lim)) {
                if (arr[x,y] > MAX_PILE) {
                    sand <- arr[x, y] %/% 4
                    arr[x,y] <- arr[x,y] %% 4

                    # up
                    if (y + 1 <= y_lim) {
                        arr[x, y+1] <- arr[x, y+1] + sand
                    }

                    # right
                    if (x + 1 <= x_lim) {
                        arr[x + 1, y] <- arr[x + 1, y] + sand
                    }

                    # down
                    if (y - 1 > 0) {
                        arr[x, y - 1] <- arr[x, y - 1] + sand
                    }

                    # left
                    if (x - 1 > 0) {
                        arr[x - 1, y] <- arr[x - 1, y] + sand
                    }
                }
            }
        }
    }
    return(arr)
}

addSandToMiddle <- function(arr, amount) {
    shape <- dim(arr)
    x <- shape[1]
    y <- shape[2]
    arr[ (x + 1) %/% 2, (y + 1) %/% 2] <- amount
    return (arr)
}