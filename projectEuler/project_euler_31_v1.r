# https://projecteuler.net/problem=31

ptm <- proc.time()

getCombinationCount <- function(total, arr) {
    possibleCombinations <<- 0

    reduce <- function(total, arr, index=1) {
        item <- arr[index]
        index <- index + 1

        if (total == 0) {
            possibleCombinations <<- possibleCombinations + 1
            return ()
        }

        if (index > length(arr)) {
            if (total %% item == 0) {
                possibleCombinations <<- possibleCombinations + 1
            }
            return ()
        }
        if (item > total) {
            return (reduce(total, arr, index))
        }

        while (TRUE) {
            reduce(total, arr, index)
            total <- total - item
            if (total < 0) {
                break
            }
        }
    }

    reduce(total, arr)
    return (possibleCombinations)
}

print(getCombinationCount(500, c(200, 100, 50, 20, 10, 5, 2, 1)))

print(proc.time() - ptm)

# [1] 73682
#    user  system elapsed
#    0.16    0.00    0.16
