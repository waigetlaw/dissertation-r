# Remove int -> str -> int casting

# https://projecteuler.net/problem=92

# A number chain is created by continuously adding the square of the digits in a number to form a new number until it has been seen before.

# For example,

# 44 -> 32 -> 13 -> 10 -> 1 -> 1
# 85 -> 89 -> 145 -> 42 -> 20 -> 4 -> 16 -> 37 -> 58 -> 89

# Therefore any chain that arrives at 1 or 89 will become stuck in an endless loop. What is most amazing is that EVERY starting number will eventually arrive at 1 or 89.

# How many starting numbers below ten million will arrive at 89?

# Answer: 8581146

ptm <- proc.time()

test <- function(num) {
    while (num != 1 && num !=89) {
        square_sum <- 0
        while (num > 0) {
            square_sum <- square_sum + (num %% 10) ^ 2
            num <- num %/% 10
        }
        num <- square_sum
    }
    return(num == 89)
}

range <- (1:10000000)

s <- sapply(range, test)

print(sum(s))

print(proc.time() - ptm)

#    user  system elapsed 
#   95.35    0.08   95.44
