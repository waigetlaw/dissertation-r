# https://projecteuler.net/problem=55


reverseNumber <- function(n) {
    return (
        as.numeric(
            paste(rev(strsplit(as.character(n),"")[[1]]),collapse="")
            )
        )
}

isPalindromicNumber <- function(n) {
        reverse <- reverseNumber(n)
        return (!is.na(reverse) && n == reverse)
}

isLychrel <- function(n) {
    n <- n + reverseNumber(n)
    count <- 0
    while (!(isPalindromicNumber(n))) {
        n <- n + reverseNumber(n)
        count = count + 1
        if (count > 50) {
            return(1)
        }
    }
    return(0)
}

countLychrel <- function() {
    range <- (1:10000)
    s <- sapply(range, isLychrel)
    return (sum(s))
}

ptm <- proc.time()

print(countLychrel())

print(proc.time() - ptm)

# [1] 249
#    user  system elapsed 
#    1.60    0.00    1.59
