# https://projecteuler.net/problem=92

# A number chain is created by continuously adding the square of the digits in a number to form a new number until it has been seen before.

# For example,

# 44 -> 32 -> 13 -> 10 -> 1 -> 1
# 85 -> 89 -> 145 -> 42 -> 20 -> 4 -> 16 -> 37 -> 58 -> 89

# Therefore any chain that arrives at 1 or 89 will become stuck in an endless loop. What is most amazing is that EVERY starting number will eventually arrive at 1 or 89.

# How many starting numbers below ten million will arrive at 89?

# Answer: 8581146

ptm <- proc.time()

test <- function(num) {
    while (num != 1 && num !=89) {
        x <- as.character(num)
        digits <- as.numeric(unlist(strsplit(x, "")))
        digits <- digits^2
        num <- sum(digits)
    }
    if (num == 89) {
        return(1)
    }
    return(0)
}

range <- (1:10000000)

s <- sapply(range, test)

print(sum(s))

print(proc.time() - ptm)

#    user  system elapsed 
#  541.29    0.15  542.25
